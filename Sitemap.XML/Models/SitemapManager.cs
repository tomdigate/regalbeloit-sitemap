﻿/* *********************************************************************** *
 * File   : SitemapManager.cs                             Part of Sitecore *
 * Version: 1.0.0                                         www.sitecore.net *
 *                                                                         *
 *                                                                         *
 * Purpose: Manager class what contains all main logic                     *
 *                                                                         *
 * Copyright (C) 1999-2009 by Sitecore A/S. All rights reserved.           *
 *                                                                         *
 * This work is the property of:                                           *
 *                                                                         *
 *        Sitecore A/S                                                     *
 *        Meldahlsgade 5, 4.                                               *
 *        1613 Copenhagen V.                                               *
 *        Denmark                                                          *
 *                                                                         *
 * This is a Sitecore published work under Sitecore's                      *
 * shared source license.                                                  *
 *                                                                         *
 * *********************************************************************** */

using System.Web.UI;
using Sitecore;
using Sitecore.Buckets.Managers;
using Sitecore.Configuration;
using Sitecore.ContentSearch;
using Sitecore.ContentSearch.Linq;
using Sitecore.ContentSearch.SearchTypes;
using Sitecore.Data;
using Sitecore.Data.Fields;
using Sitecore.Data.Items;
using Sitecore.Diagnostics;
using Sitecore.Sites;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using Sitecore.Links;

using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Security.Permissions;
using Sitecore.Resources.Media;
using Regal.Main.Web.SC.Models.sitecore.templates.Project.Regal.Page_Types.News;
using Glass.Mapper.Sc;
using Regal.Main.Web.SC.Models.sitecore.templates.Project.Regal.Page_Types.Events;
using Sitecore.ContentSearch.Converters;

namespace Sitemap.XML.Models
{
    public class RglbCommerceBaseSearchResultItem : SearchResultItem
    {
        [IndexField("commercesearchitemtype")]
        public string CommerceSearchItemType { get; set; }
        [IndexField("brand")]
        public string Brand { get; set; }
        [IndexField("catalogno")]
        public string CatalogNumber { get; set; }
        [IndexField("seourls")]
        [TypeConverter(typeof(IndexFieldEnumerableConverter))]
        public IEnumerable<string> SeoUrls { get; set; }
        [IndexField("_displayname")]
        public string DisplayName { get; set; }
        [IndexField("haschildren")]
        public bool HasChildren { get; set; }
        [IndexField("modelno")]
        public string ModelNumber { get; set; }
        [IndexField("partid")]
        public string PartId { get; set; }
    }
    public class RglbDocument
    {
        public virtual DateTime Updated { get; set; }
        public string SeoUrl { get; set; }
        public string DisplayName { get; set; }
        public string ModelNumber { get; set; }
        public string CatalogNumber { get; set; }
        public string Brand { get; set; }
        public string PartId { get; set; }
    }
    public class SitemapManager
    {
        #region Fields 

        private readonly SitemapManagerConfiguration _config;
        private readonly string SxaPageSitemapField = "NavigationFilter";
        private readonly string LegacyPageSitemapField = "IsSitemap";
        private readonly string CommerceWebIndex = "commerce_web_index";
        private readonly string ProductsFolder = "/products";
        #endregion

        #region Constructor

        public SitemapManager(SitemapManagerConfiguration config)
        {
            Assert.IsNotNull(config, "config");
            _config = config;
            if (!string.IsNullOrWhiteSpace(_config.FileName))
            {
                BuildSiteMap();
            }
        }

        #endregion

        #region Properties

        public Database Db
        {
            get
            {
                Database database = Factory.GetDatabase(SitemapManagerConfiguration.WorkingDatabase);
                return database;
            }
        }

        #endregion

        #region Private Methods

        private static IEnumerable<Item> GetSharedContentDefinitions()
        {
            var siteNode = GetContextSiteDefinitionItem();
            if (siteNode == null || string.IsNullOrWhiteSpace(siteNode.Name)) return null;

            var sharedDefinitions = siteNode.Children;
            return sharedDefinitions;
        }

        private static Item GetContextSiteDefinitionItem()
        {
            var database = Context.Database;
#if DEBUG
            database = Factory.GetDatabase("master");
#endif
            var sitemapModuleItem = database.GetItem(Constants.SitemapModuleSettingsRootItemId);
            var contextSite = Context.GetSiteName().ToLower();
            if (!sitemapModuleItem.Children.Any()) return null;
            var siteNode = sitemapModuleItem.Children.FirstOrDefault(i => i.Key == contextSite);
            return siteNode;
        }

        private string BuildSitemapXML(List<SitemapItem> items, Site site, IEnumerable<RglbDocument> plps)
        {
            XmlDocument doc = new XmlDocument();

            XmlNode declarationNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(declarationNode);
            XmlNode urlsetNode = doc.CreateElement("urlset");
            XmlAttribute xmlnsAttr = doc.CreateAttribute("xmlns");
            xmlnsAttr.Value = SitemapManagerConfiguration.XmlnsTpl;
            urlsetNode.Attributes.Append(xmlnsAttr);

            doc.AppendChild(urlsetNode);

            SitecoreService service = new SitecoreService(Sitecore.Context.ContentDatabase);
            var news = Regal.Main.Web.SC.Commons.Constants.NewsListID;
            var events = Regal.Main.Web.SC.Commons.Constants.EventsListID;
            Sitecore.Diagnostics.Log.Info(string.Format("Sitemap Content Pages Total Count :: \"{0}\"", items.Count()), this);
            foreach (var itm in items)
            {
                if (itm.ParentGuid == news)
                {
                    News_Article obj = service.Cast<News_Article>(itm.ThisItem);
                    if (obj.Inactive == false && obj.Publish_Date.Date <= DateTime.Today && obj.End_Date.Date >= DateTime.Today)
                        doc = this.BuildSitemapItem(doc, itm, site);
                }
                else if (itm.ParentGuid == events)
                {
                    Event_Article obj = service.Cast<Event_Article>(itm.ThisItem);
                    if (obj.Inactive == false && obj.End_Date.Date >= DateTime.Today)
                        doc = this.BuildSitemapItem(doc, itm, site);
                }
                else
                    doc = this.BuildSitemapItem(doc, itm, site);
            }

            try
            {
                var serverUrl = (new SitemapManagerConfiguration(site.Name)).ServerUrl;
                if (plps != null)
                {
                    Sitecore.Diagnostics.Log.Info(string.Format("Sitemap PLP Total Count :: \"{0}\"", plps.Count()), this);
                    foreach (var plp in plps)
                    {
                        BuildProductsSitemapItem(doc, serverUrl + ProductsFolder + plp.SeoUrl, plp.Updated.ToString("yyyy-MM-dd"));
                    }
                }
            }
            catch (System.Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(string.Format("Sitemap Products url adding :: \"{0}\"", ex.Message), this);
            }
            return IncludeIndentation(doc.OuterXml);
        }
        private string BuildCompleteSitemapXML(List<SitemapItem> items, Site site, IEnumerable<RglbDocument> plps, RglbDocument pdp)
        {
            XmlDocument doc = new XmlDocument();

            XmlNode declarationNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(declarationNode);
            XmlNode urlsetNode = doc.CreateElement("urlset");
            XmlAttribute xmlnsAttr = doc.CreateAttribute("xmlns");
            xmlnsAttr.Value = SitemapManagerConfiguration.XmlnsTpl;
            urlsetNode.Attributes.Append(xmlnsAttr);

            doc.AppendChild(urlsetNode);

            SitecoreService service = new SitecoreService(Sitecore.Context.ContentDatabase);
            var news = Regal.Main.Web.SC.Commons.Constants.NewsListID;
            var events = Regal.Main.Web.SC.Commons.Constants.EventsListID;
            Sitecore.Diagnostics.Log.Info(string.Format("Sitemap Content Pages Total Count :: \"{0}\"", items.Count()), this);
            foreach (var itm in items)
            {
                if (itm.ParentGuid == news)
                {
                    News_Article obj = service.Cast<News_Article>(itm.ThisItem);
                    if (obj.Inactive == false && obj.Publish_Date.Date <= DateTime.Today && obj.End_Date.Date >= DateTime.Today)
                        doc = this.BuildSitemapItem(doc, itm, site);
                }
                else if (itm.ParentGuid == events)
                {
                    Event_Article obj = service.Cast<Event_Article>(itm.ThisItem);
                    if (obj.Inactive == false && obj.End_Date.Date >= DateTime.Today)
                        doc = this.BuildSitemapItem(doc, itm, site);
                }
                else
                    doc = this.BuildSitemapItem(doc, itm, site);
            }

            try
            {
                var serverUrl = (new SitemapManagerConfiguration(site.Name)).ServerUrl;
                if (plps != null)
                {
                    Sitecore.Diagnostics.Log.Info(string.Format("Sitemap PLP Total Count :: \"{0}\"", plps.Count()), this);
                    foreach (var plp in plps)
                    {
                        BuildProductsSitemapItem(doc, serverUrl + ProductsFolder + plp.SeoUrl, plp.Updated.ToString("yyyy-MM-dd"));
                    }
                }
            }
            catch (System.Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(string.Format("Sitemap Products url adding :: \"{0}\"", ex.Message), this);
            }
            try
            {
                var serverUrl = (new SitemapManagerConfiguration(site.Name)).ServerUrl;
                if (pdp != null)
                {
                    BuildProductsSitemapItem(doc, serverUrl + ProductsFolder + pdp.SeoUrl, pdp.Updated.ToString("yyyy-MM-dd"));
                }
            }
            catch (System.Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(string.Format("Sitemap Products url adding :: \"{0}\"", ex.Message), this);
            }
            return IncludeIndentation(doc.OuterXml);
        }
        private string BuildSitemapIndex(int noOfSitemap)
        {
            XmlDocument doc = new XmlDocument();

            XmlNode declarationNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(declarationNode);
            XmlNode sitemapindex = doc.CreateElement("sitemapindex");
            XmlAttribute xmlnsAttr = doc.CreateAttribute("xmlns");
            xmlnsAttr.Value = SitemapManagerConfiguration.XmlnsTpl;
            sitemapindex.Attributes.Append(xmlnsAttr);

            doc.AppendChild(sitemapindex);
            XmlNode urlsetNode = doc.LastChild;


            for (int i = 1; i <= noOfSitemap; i++)
            {
                XmlNode sitemapNode = doc.CreateElement("sitemap");
                urlsetNode.AppendChild(sitemapNode);
                XmlNode locNode = doc.CreateElement("loc");
                sitemapNode.AppendChild(locNode);
                string loc = _config.ServerUrl + "/-/media/Project CMS/Regal/Sitemap/xml/" + "sitemap" + i + ".xml";
                locNode.AppendChild(doc.CreateTextNode(loc));
            }

            return IncludeIndentation(doc.OuterXml);
        }
        private string BuildSitemapXMLPDP(Site site, IEnumerable<RglbDocument> pdps)
        {
            XmlDocument doc = new XmlDocument();
            XmlNode declarationNode = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            doc.AppendChild(declarationNode);
            XmlNode urlsetNode = doc.CreateElement("urlset");
            XmlAttribute xmlnsAttr = doc.CreateAttribute("xmlns");
            xmlnsAttr.Value = SitemapManagerConfiguration.XmlnsTpl;
            urlsetNode.Attributes.Append(xmlnsAttr);

            doc.AppendChild(urlsetNode);

            SitecoreService service = new SitecoreService(Sitecore.Context.ContentDatabase);
            try
            {
                var serverUrl = (new SitemapManagerConfiguration(site.Name)).ServerUrl;
                if (pdps != null)
                {
                    Sitecore.Diagnostics.Log.Info(string.Format("Sitemap PDP Total Count :: \"{0}\"", pdps.Count()), this);
                    foreach (var product in pdps)
                    {
                        BuildProductsSitemapItem(doc, serverUrl + ProductsFolder + product.SeoUrl, product.Updated.ToString("yyyy-MM-dd"));

                    }
                }
            }
            catch (System.Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(string.Format("Sitemap Products url adding :: \"{0}\"", ex.Message), this);
            }
            return IncludeIndentation(doc.OuterXml);
        }
        private XmlDocument BuildProductsSitemapItem(XmlDocument doc, string url, string lastMod)
        {
            XmlNode urlsetNode = doc.LastChild;

            XmlNode urlNode = doc.CreateElement("url");
            urlsetNode.AppendChild(urlNode);

            XmlNode locNode = doc.CreateElement("loc");
            urlNode.AppendChild(locNode);
            locNode.AppendChild(doc.CreateTextNode(url));

            XmlNode lastmodNode = doc.CreateElement("lastmod");
            urlNode.AppendChild(lastmodNode);
            lastmodNode.AppendChild(doc.CreateTextNode(lastMod));

            return doc;
        }
        private XmlDocument BuildSitemapItem(XmlDocument doc, SitemapItem item, Site site)
        {
            XmlNode urlsetNode = doc.LastChild;

            XmlNode urlNode = doc.CreateElement("url");
            urlsetNode.AppendChild(urlNode);

            XmlNode locNode = doc.CreateElement("loc");
            urlNode.AppendChild(locNode);
            string loc = item.Location.StartsWith("http:") ? item.Location.Replace("http:", "https:") : item.Location;
            locNode.AppendChild(doc.CreateTextNode(loc));

            XmlNode lastmodNode = doc.CreateElement("lastmod");
            urlNode.AppendChild(lastmodNode);
            lastmodNode.AppendChild(doc.CreateTextNode(item.LastModified));

            if (!string.IsNullOrWhiteSpace(item.ChangeFrequency))
            {
                XmlNode changeFrequencyNode = doc.CreateElement("changefreq");
                urlNode.AppendChild(changeFrequencyNode);
                changeFrequencyNode.AppendChild(doc.CreateTextNode(item.ChangeFrequency));
            }

            if (!string.IsNullOrWhiteSpace(item.Priority))
            {
                var priorityNode = doc.CreateElement("priority");
                urlNode.AppendChild(priorityNode);
                var pItem = Factory.GetDatabase("web").GetItem(new ID(item.Priority));
                if (pItem != null)
                    priorityNode.AppendChild(doc.CreateTextNode(pItem.DisplayName));
            }

            return doc;
        }

        private string BuildSitemapHTML(List<SitemapItem> items, Site site, IEnumerable<RglbDocument> plps, IEnumerable<RglbDocument> pdps)
        {
            StringBuilder htmlStr = new StringBuilder("");
            htmlStr.Append("<link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css'>");
            htmlStr.Append("<table class='table table-striped' style='width:100%;'><tbody><tr>");
            var count = 1;

            SitecoreService service = new SitecoreService(Sitecore.Context.ContentDatabase);
            var news = Regal.Main.Web.SC.Commons.Constants.NewsListID;
            var events = Regal.Main.Web.SC.Commons.Constants.EventsListID;
            foreach (var item in items)
            {
                string location = item.Location.StartsWith("http:") ? item.Location.Replace("http:", "https:") : item.Location;
                string title = item.Title ?? item.DisplayName;
                if (item.ParentGuid == news)
                {
                    News_Article obj = service.Cast<News_Article>(item.ThisItem);
                    if (obj.Inactive == false && obj.Publish_Date.Date <= DateTime.Today && obj.End_Date.Date >= DateTime.Today)
                    {
                        htmlStr.Append("<td style='width:33%;'><a target='_blank' href='" + location + "'>" + title + "</a></td>");
                        if (count % 3 == 0) htmlStr.Append("</tr><tr>");
                        count++;
                    }
                }
                else if (item.ParentGuid == events)
                {
                    Event_Article obj = service.Cast<Event_Article>(item.ThisItem);
                    if (obj.Inactive == false && obj.End_Date.Date >= DateTime.Today)
                    {
                        htmlStr.Append("<td style='width:33%;'><a target='_blank' href='" + location + "'>" + title + "</a></td>");
                        if (count % 3 == 0) htmlStr.Append("</tr><tr>");
                        count++;
                    }
                }
                else
                {
                    htmlStr.Append("<td style='width:33%;'><a target='_blank' href='" + location + "'>" + title + "</a></td>");
                    if (count % 3 == 0) htmlStr.Append("</tr><tr>");
                    count++;
                }
            }
            var serverUrl = (new SitemapManagerConfiguration(site.Name)).ServerUrl;
            if (plps != null)
            {
                foreach (var plp in plps)
                {
                    htmlStr.Append("<td style='width:33%;'><a target='_blank' href='" + serverUrl + ProductsFolder + plp.SeoUrl + "'>" + plp.DisplayName + "</a></td>");
                    if (count % 3 == 0) htmlStr.Append("</tr><tr>");
                    count++;
                }
            }
            if (pdps != null)
            {
                foreach (var product in pdps)
                {
                    htmlStr.Append("<td style='width:33%;'><a target='_blank' href='" + serverUrl + ProductsFolder + product.SeoUrl + "'>" +
                        (product.Brand + " " + product.DisplayName +
                        (!string.IsNullOrWhiteSpace(product.ModelNumber) ? " - " + product.ModelNumber :
                        (!string.IsNullOrWhiteSpace(product.CatalogNumber) ? " - " + product.CatalogNumber :
                        (!string.IsNullOrWhiteSpace(product.PartId) ? " - " + product.PartId : string.Empty)))).Trim() + "</a></td>");

                    if (count % 3 == 0) htmlStr.Append("</tr><tr>");
                    count++;
                }
            }

            htmlStr.Append("</tr></tbody></table>");
            return System.Convert.ToString(htmlStr);
        }
        private void SubmitEngine(string engine, string sitemapUrl)
        {
            //Check if it is not localhost because search engines returns an error
            if (!sitemapUrl.Contains("http://localhost"))
            {
                string request = string.Concat(engine, SitemapItem.HtmlEncode(sitemapUrl));

                System.Net.HttpWebRequest httpRequest =
                    (System.Net.HttpWebRequest)System.Net.HttpWebRequest.Create(request);
                try
                {
                    System.Net.WebResponse webResponse = httpRequest.GetResponse();

                    System.Net.HttpWebResponse httpResponse = (System.Net.HttpWebResponse)webResponse;
                    if (httpResponse.StatusCode != System.Net.HttpStatusCode.OK)
                    {
                        Sitecore.Diagnostics.Log.Error(string.Format("Cannot submit sitemap to \"{0}\"", engine), this);
                    }
                }
                catch
                {
                    Sitecore.Diagnostics.Log.Warn(string.Format("Sitemap The serachengine \"{0}\" returns an 404 error", request), this);
                }
            }
        }

        private List<RglbDocument> SearchPDPProducts()
        {
            try
            {
                var pdpItem = Factory.GetDatabase("web").GetItem("/sitecore/content/RGLB/RegalBeloit/Home" + ProductsFolder + "/*/*");
                List<RglbDocument> pdps = new List<RglbDocument>(); int startPage = 0; int retCount;

                if (pdpItem != null && pdpItem.Fields[SxaPageSitemapField] != null && CheckToHideInNavigationFilters(pdpItem.Fields[SxaPageSitemapField]))
                {
                    var searchIndex = ContentSearchManager.GetIndex(CommerceWebIndex);
                    do
                    {
                        using (var context = searchIndex.CreateSearchContext())
                        {
                            var searchResults = context.GetQueryable<RglbCommerceBaseSearchResultItem>().Where(item =>
                                item.CommerceSearchItemType == "SellableItem"
                                && item["_latestversion"].Equals("1")
                                && item.Path.Contains("Productlines")
                                && item.Language == "en").Page(startPage, 1000);
                            var results = searchResults.GetResults();
                            retCount = results.Hits.Count(); startPage++;
                            foreach (var i in results.Hits)
                            {
                                var doc = i.Document;
                                if (doc.Updated != DateTime.MinValue)
                                {
                                    pdps.Add(new RglbDocument()
                                    {
                                        DisplayName = doc.DisplayName,
                                        SeoUrl = doc.SeoUrls.FirstOrDefault(),
                                        Updated = doc.Updated,
                                        ModelNumber = doc.ModelNumber,
                                        CatalogNumber = doc.CatalogNumber,
                                        Brand = doc.Brand,
                                        PartId = doc.PartId
                                    });
                                }
                            }
                        }
                        Sitecore.Diagnostics.Log.Info(string.Format("Sitemap PDP :: Page : {0} Count : {1}", startPage, retCount), this);
                    } while (retCount == 1000);
                    return pdps;
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(string.Format("Sitemap PDP Issue :: \"{0}\"", ex.Message), this);
                return null;
            }
            return null;
        }
        private List<RglbDocument> SearchPLPProducts()
        {
            try
            {
                var plpItem = Factory.GetDatabase("web").GetItem("/sitecore/content/RGLB/RegalBeloit/Home" + ProductsFolder + "/*");
                List<RglbDocument> plps = new List<RglbDocument>(); int startPage = 0; int retCount;

                if (plpItem != null && plpItem.Fields[SxaPageSitemapField] != null && CheckToHideInNavigationFilters(plpItem.Fields[SxaPageSitemapField]))
                {
                    var searchIndex = ContentSearchManager.GetIndex(CommerceWebIndex);
                    do
                    {
                        using (var context = searchIndex.CreateSearchContext())
                        {
                            var searchResults = context.GetQueryable<RglbCommerceBaseSearchResultItem>().Where(item =>
                                item.CommerceSearchItemType == "Category" && item["_latestversion"].Equals("1") && item.Path.Contains("Productlines")
                                && item.HasChildren && item.Language == "en").Page(startPage, 1000);
                            var results = searchResults.GetResults();
                            retCount = results.Hits.Count(); startPage++;
                            foreach (var i in results.Hits)
                            {
                                var doc = i.Document;
                                plps.Add(new RglbDocument() { DisplayName = doc.DisplayName, SeoUrl = doc.SeoUrls.FirstOrDefault(), Updated = doc.Updated });
                            }
                        }
                        Sitecore.Diagnostics.Log.Info(string.Format("Sitemap PLP :: Page : {0} Count : {1}", startPage, retCount), this);
                    } while (retCount == 1000);
                    return plps;
                }
            }
            catch (Exception ex)
            {
                Sitecore.Diagnostics.Log.Error(string.Format("Sitemap PLP Issue :: \"{0}\"", ex.Message), this);
                return null;
            }
            return null;
        }
        private void BuildSiteMap()
        {
            Site site = Sitecore.Sites.SiteManager.GetSite(_config.SiteName);
            SiteContext siteContext = Factory.GetSite(_config.SiteName);
            string rootPath = siteContext.StartPath;
            Database targetDB = Factory.GetDatabase("web");

            // GET ALL Tree Items
            List<SitemapItem> treeItems = GetSitemapItems(rootPath);
            // GET ALL PLPs
            var plps = SearchPLPProducts();
            // GET ALL PDPs
            var pdps = SearchPDPProducts();


            string scItemsXMLContent = this.BuildSitemapXML(treeItems, site, plps);
            string completeXMLContent = this.BuildCompleteSitemapXML(treeItems, site, plps, pdps != null ? pdps.First() : null);

            var pdpChunks = BuildChunks(pdps, 40000);
            string XMLIndexContent = this.BuildSitemapIndex(pdpChunks.Count + 1);
            List<string> pdpXMLList = new List<string>(10);


            foreach (var pdpChunk in pdpChunks)
            {
                pdpXMLList.Add(this.BuildSitemapXMLPDP(site, pdpChunk));
            }
            string completeXMLFullPath = MainUtil.MapPath(string.Concat("/", _config.FileName, ".xml"));
            string scItemsXMLFullPath = MainUtil.MapPath(string.Concat("/", "sitemap1", ".xml"));
            string XMLIndexFullPath = MainUtil.MapPath(string.Concat("/", "sitemap_index", ".xml"));
            using (StreamWriter strWriter = new StreamWriter(completeXMLFullPath, false))
            {
                strWriter.Write(completeXMLContent);
                strWriter.Close();
            }
            using (StreamWriter strWriter = new StreamWriter(scItemsXMLFullPath, false))
            {
                strWriter.Write(scItemsXMLContent);
                strWriter.Close();
            }
            using (StreamWriter strWriter = new StreamWriter(XMLIndexFullPath, false))
            {
                strWriter.Write(XMLIndexContent);
                strWriter.Close();
            }

            int sitemapCount = 2;
            List<string> xmlPaths = new List<string>(10);
            foreach (var item in pdpXMLList)
            {
                xmlPaths.Add(MainUtil.MapPath(string.Concat("/", "sitemap" + sitemapCount, ".xml")));
                using (StreamWriter strWriter = new StreamWriter(xmlPaths.Last(), false))
                {
                    strWriter.Write(item);
                    strWriter.Close();
                }
                sitemapCount++;
            }


            string htmlFullPath = MainUtil.MapPath(string.Concat("/", _config.FileName, ".html"));
            string htmlContent = this.BuildSitemapHTML(treeItems, site, plps, pdps);

            using (StreamWriter strWriter = new StreamWriter(htmlFullPath, false))
            {
                strWriter.Write(htmlContent);
                strWriter.Close();
            }
            int sitemapFileCount = 2;
            AddFile(completeXMLFullPath, "/sitecore/media library/Project CMS/Regal/Sitemap/xml", _config.FileName);
            AddFile(XMLIndexFullPath, "/sitecore/media library/Project CMS/Regal/Sitemap/xml", "sitemap_index");
            AddFile(scItemsXMLFullPath, "/sitecore/media library/Project CMS/Regal/Sitemap/xml", "sitemap1");
            foreach (var path in xmlPaths)
            {
                AddFile(path, "/sitecore/media library/Project CMS/Regal/Sitemap/xml", "sitemap" + sitemapFileCount);
                sitemapFileCount++;
            }
            //XML CMS         
            AddFile(htmlFullPath, "/sitecore/media library/Project CMS/Regal/Sitemap/html", _config.FileName);//HTML CMS
            //AddFile(xmlFullPath, "/sitecore/media library/Project/Regal/Sitemap/xml", "sitemap"); //XML OLD        
            //AddFile(htmlFullPath, "/sitecore/media library/Project/Regal/Sitemap/html", "sitemap");//HTML OLD

            using (new Sitecore.SecurityModel.SecurityDisabler())
            {
                Database webdb = Sitecore.Configuration.Factory.GetDatabase("web");
                Database masterdb = Sitecore.Configuration.Factory.GetDatabase("master");

                //ClearSitecoreDatabaseCache(masterdb);
                Item masterCMSItem = masterdb.GetItem("/sitecore/media library/Project CMS/Regal/Sitemap/");
                // target databases
                Database[] databases = new Database[1] { webdb };
                Sitecore.Handle publishCMSHandle = Sitecore.Publishing.PublishManager.PublishItem(masterCMSItem, databases, webdb.Languages, true, false);
                //ClearSitecoreDatabaseCache(webdb);
                Sitecore.Diagnostics.Log.Info("Sitemap Publish Completed.", this);
            }
        }
        private void AddFile(string fileName, string sitecorePath, string mediaItemName)
        {
            MediaCreatorOptions options = new MediaCreatorOptions();// Create the options            
            options.FileBased = false;// Store the file in the database, not as a file           
            options.IncludeExtensionInItemName = false; // Remove file extension from item name            
            //options.OverwriteExisting = true;// Overwrite any existing file with the same name            
            options.Versioned = false;// Do not make a versioned template           
            options.Destination = sitecorePath + "/" + mediaItemName; // set the path            
            options.Database = Factory.GetDatabase("master");// Set the database
            // Now create the file
            MediaCreator creator = new MediaCreator();
            MediaItem mediaItem = creator.CreateFromFile(fileName, options);
            Sitecore.Diagnostics.Log.Info("Sitemap uploaded to Database :" + fileName, this);
        }
        public void ClearSitecoreDatabaseCache(Database db)
        {
            // clear html cache
            Sitecore.Context.Site.Caches.HtmlCache.Clear();

            db.Caches.ItemCache.Clear();
            db.Caches.DataCache.Clear();

            //Clear prefetch cache
            foreach (var cache in Sitecore.Caching.CacheManager.GetAllCaches())
            {
                if (cache.Name.Contains(string.Format("Prefetch data({0})", db.Name)))
                {
                    cache.Clear();
                }
            }
        }

        public bool CheckToHideInNavigationFilters(MultilistField mlf)
        {
            var items = mlf.GetItems();
            if (items != null && items.Count() > 0)
            {
                foreach (Item i in items)
                {
                    return i.Name == "Sitemap Navigation" ? false : true;
                }
            }
            return true;
        }
        private List<SitemapItem> GetSitemapItems(string rootPath)
        {

            Database database = Factory.GetDatabase(SitemapManagerConfiguration.WorkingDatabase);
            Item contentRoot = database.Items[rootPath];

            IEnumerable<Item> descendants;
            Sitecore.Security.Accounts.User user = Sitecore.Security.Accounts.User.FromName(Constants.SitemapParserUser, true);
            using (new Sitecore.Security.Accounts.UserSwitcher(user))
            {
                descendants = contentRoot.Axes.GetDescendants()
                    .Where(i => (i.Fields[LegacyPageSitemapField] != null && i[LegacyPageSitemapField] == "1")
                    || (i.Fields[SxaPageSitemapField] != null && CheckToHideInNavigationFilters(i.Fields[SxaPageSitemapField])));
            }

            // getting shared content
            var sharedModels = new List<SitemapItem>();
            var sharedDefinitions = Db.SelectItems(string.Format("fast:{0}/*", _config.SitemapConfigurationItemPath));
            var site = Factory.GetSite(_config.SiteName);
            ////var enabledTemplates = BuildListFromString(disTpls, '|');
            foreach (var sharedDefinition in sharedDefinitions)
            {
                if (string.IsNullOrWhiteSpace(sharedDefinition[Constants.SharedContent.ContentLocationFieldName]) ||
                    string.IsNullOrWhiteSpace(sharedDefinition[Constants.SharedContent.ParentItemFieldName]))
                    continue;
                var contentLocation = ((DatasourceField)sharedDefinition.Fields[Constants.SharedContent.ContentLocationFieldName]).TargetItem;
                var parentItem = ((DatasourceField)sharedDefinition.Fields[Constants.SharedContent.ParentItemFieldName]).TargetItem;
                var sharedItems = new List<Item>();
                if (BucketManager.IsBucket(contentLocation))
                {
                    var index = ContentSearchManager.GetIndex(new SitecoreIndexableItem(contentLocation));
                    using (var searchContext = index.CreateSearchContext())
                    {
                        var searchResultItem =
                            searchContext.GetQueryable<SearchResultItem>()
                                .Where(item => item.Paths.Contains(contentLocation.ID) && item.ItemId != contentLocation.ID)
                                .ToList();
                        sharedItems.AddRange(searchResultItem.Select(i => i.GetItem()));
                    }
                }
                else
                {
                    sharedItems.AddRange(contentLocation.Axes.GetDescendants());
                }

                var cleanedSharedItems = from itm in sharedItems
                                         where itm.Template != null ////&& enabledTemplates.Select(t => t.ToLower()).Contains(itm.Template.ID.ToString().ToLower())
                                         select itm;
                var sharedSitemapItems = cleanedSharedItems.Select(i => new SitemapItem(i, site, parentItem));
                sharedModels.AddRange(sharedSitemapItems);
            }

            var sitemapItems = descendants.ToList();
            sitemapItems.Insert(0, contentRoot);

            var selected = from itm in sitemapItems
                           where itm.Template != null//// && enabledTemplates.Contains(itm.Template.ID.ToString())
                           select itm;

            var selectedModels = selected.Select(i => new SitemapItem(i, site, null)).ToList();
            selectedModels.AddRange(sharedModels);
            selectedModels = selectedModels.OrderBy(u => u.Priority).Take(int.Parse(Settings.GetSetting("Sitemap.XML.UrlLimit", "10000"))).ToList();////
            return selectedModels;
        }

        private static List<string> BuildListFromString(string str, char separator)
        {
            var enabledTemplates = str.Split(separator);
            var selected = from dtp in enabledTemplates
                           where !string.IsNullOrEmpty(dtp)
                           select dtp;

            var result = selected.ToList();

            return result;
        }

        private List<List<RglbDocument>> BuildChunks(List<RglbDocument> fullList, int batchSize)
        {
            int total = 0;
            var chunkedList = new List<List<RglbDocument>>();
            if (fullList != null)
            {
                while (total < fullList.Count)
                {
                    var chunk = fullList.Skip(total).Take(batchSize);
                    chunkedList.Add(chunk.ToList());
                    total += batchSize;
                }
            }

            return chunkedList;
        }


        #region View Helpers

        public static string IncludeIndentation(string xml)
        {
            string result = "";

            MemoryStream mStream = new MemoryStream();
            XmlTextWriter writer = new XmlTextWriter(mStream, Encoding.Unicode);
            XmlDocument document = new XmlDocument();

            try
            {
                // Load the XmlDocument with the XML.
                document.LoadXml(xml);
                writer.Formatting = Formatting.Indented;
                // Write the XML into a formatting XmlTextWriter
                document.WriteContentTo(writer);
                writer.Flush();
                mStream.Flush();
                // Have to rewind the MemoryStream in order to read
                // its contents.
                mStream.Position = 0;
                // Read MemoryStream contents into a StreamReader.
                StreamReader sReader = new StreamReader(mStream);
                // Extract the text from the StreamReader.
                string formattedXml = sReader.ReadToEnd();
                result = formattedXml;
            }
            catch (XmlException ex)
            {
                Sitecore.Diagnostics.Log.Error("Error While Applying Indendation:", ex);
            }
            mStream.Close();
            writer.Close();
            return result;
        }
        public static bool IsUnderContent(Item item)
        {
            return Context.Database.GetItem(Context.Site.StartPath).Axes.IsAncestorOf(item);
        }

        public static bool IsShared(Item item)
        {
            var sharedDefinitions = GetSharedContentDefinitions();
            if (sharedDefinitions == null) return false;
            var sharedItemContentRoots =
                sharedDefinitions.Select(i => ((DatasourceField)i.Fields[Constants.SharedContent.ParentItemFieldName]).TargetItem).ToList();
            if (!sharedItemContentRoots.Any()) return false;

            return sharedItemContentRoots.Any(i => i.ID == item.ID);
        }

        public static bool SitemapDefinitionExists()
        {
            var sitemapModuleSettingsItem = Context.Database.GetItem(Constants.SitemapModuleSettingsRootItemId);
            var siteDefinition = sitemapModuleSettingsItem.Children[Context.Site.Name];
            return siteDefinition != null;
        }

        public static Item GetContentLocation(Item item)
        {
            var sharedNodes = GetSharedContentDefinitions();
            var contentParent = sharedNodes
                .Where(n => ((DatasourceField)n.Fields[Constants.SharedContent.ContentLocationFieldName]).TargetItem.Axes.IsAncestorOf(item))
                .Select(n => ((DatasourceField)n.Fields[Constants.SharedContent.ContentLocationFieldName]).TargetItem)
                .FirstOrDefault();
            return contentParent;
        }

        public static bool IsChildUnderSharedLocation(Item child)
        {
            var sharedNodes = GetSharedContentDefinitions();
            var sharedContentLocations = sharedNodes.Select(n => ((DatasourceField)n.Fields[Constants.SharedContent.ContentLocationFieldName]).TargetItem);
            var isUnderShared = sharedContentLocations.Any(l => l.Axes.IsAncestorOf(child));
            return isUnderShared;
        }

        public static Item GetSharedLocationParent(Item child)
        {
            var sharedNodes = GetSharedContentDefinitions();
            var parent = sharedNodes
                .Where(n => ((DatasourceField)n.Fields[Constants.SharedContent.ContentLocationFieldName]).TargetItem.Axes.IsAncestorOf(child))
                .Select(n => ((DatasourceField)n.Fields[Constants.SharedContent.ParentItemFieldName]).TargetItem)
                .FirstOrDefault();
            return parent;
        }

        public static bool IsEnabledTemplate(Item item)
        {
            var config = new SitemapManagerConfiguration(Context.GetSiteName());
            return config.EnabledTemplates.ToLower().Contains(item.TemplateID.ToGuid().ToString());
        }

        public static bool IsExcludedItem(Item item)
        {
            return item[Settings.GetSetting("Sitemap.XML.Fields.ExcludeItemFromSitemap", "Exclude From Sitemap")] == "1";
        }

        public static bool ContainsItemsToShow(IEnumerable<Item> items)
        {
            return items == null
                ? false
                : items.Any() && items.Any(IsEnabledTemplate) && items.Count(IsExcludedItem) < items.Count();
        }

        #endregion

        #endregion

        #region Public Members
        public bool SubmitSitemapToSearchenginesByHttp()
        {
            if (!SitemapManagerConfiguration.IsProductionEnvironment)
                return false;

            bool result = false;
            Item sitemapConfig = Db.Items[_config.SitemapConfigurationItemPath];

            if (sitemapConfig != null)
            {
                //TODO: URL
                string engines = sitemapConfig.Fields[Constants.WebsiteDefinition.SearchEnginesFieldName].Value;
                var filePath = !_config.ServerUrl.EndsWith("/")
                            ? _config.ServerUrl + "/" + _config.FileName
                            : _config.ServerUrl + _config.FileName;
                foreach (string id in engines.Split('|'))
                {
                    Item engine = Db.Items[id];
                    if (engine != null)
                    {
                        string engineHttpRequestString = engine.Fields[Constants.SitemapSubmissionUriFieldName].Value;
                        SubmitEngine(engineHttpRequestString, filePath);
                    }
                }
                result = true;
            }

            return result;
        }

        public void RegisterSitemapToRobotsFile()
        {
            string robotsPath = MainUtil.MapPath(string.Concat("/", Constants.RobotsFileName));
            StringBuilder sitemapContent = new StringBuilder(string.Empty);
            if (File.Exists(robotsPath))
            {
                StreamReader sr = new StreamReader(robotsPath);
                sitemapContent.Append(sr.ReadToEnd());
                sr.Close();
            }

            StreamWriter sw = new StreamWriter(robotsPath, false);
            string sitemapLine = string.Concat("Sitemap: ", _config.FileName);
            if (!sitemapContent.ToString().Contains(sitemapLine))
            {
                sitemapContent.AppendLine(sitemapLine);
            }
            sw.Write(sitemapContent.ToString());
            sw.Close();
        }
        #endregion
    }
}